# Will Presley's Dotfiles

## Parts of...
* List of sources is coming, I want to be thorough. Mostly <a href="https://github.com/paulirish/dotfiles" target="_blank">Paul Irish</a> though.
* ...

## Linux
* Ubuntu/Lubuntu

<https://bitbucket.org/willpresley/dotfiles/src/master/linux/>

## Windows
* WSL2

<https://bitbucket.org/willpresley/dotfiles/src/master/win/>
